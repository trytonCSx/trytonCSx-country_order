# Copyright 2021 Hartmut Goebel <h.goebel@crazy-compilers.com>
# Licensed under the GNU General Public License v3 or later (GPLv3+)
# SPDX-License-Identifier: GPL-3.0-or-later

from trytond.pool import Pool
from . import country_precedence

__all__ = ['register']

def register():
    Pool.register(
        country_precedence.Country,
        module='country_order', type_='model',
        depends=("country",))
